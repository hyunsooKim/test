package chapter09.example;

public class Car {

    public class Tire{
        public void method(){
            System.out.println("Tire 클래스 실행");
        }
    }
    public static class Engine{
        public void method(){
            System.out.println("Engine 클래스 실행");
        }
    }
}
