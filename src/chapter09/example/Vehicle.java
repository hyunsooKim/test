package chapter09.example;

public interface Vehicle {
    public abstract void run();
}
