package chapter13.example;

public class ChildPair<T,M> extends Pair<T,M>{
    public ChildPair(T key, M value){
        super(key,value);
    }
}
