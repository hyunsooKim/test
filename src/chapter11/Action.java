package chapter11;

public interface Action {
    public abstract void execute();
}
