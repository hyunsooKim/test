package chapter07.example;

public class SnowTire extends Tire{

    @Override
    public void run() {
       System.out.println("스노우 타이어 굴러갑니다.");
    }
}
