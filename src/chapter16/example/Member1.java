package chapter16.example;

public class Member1 {
    private String name;
    private String job;
    public Member1(String name, String job){
        this.name = name;
        this.job = job;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }
}
