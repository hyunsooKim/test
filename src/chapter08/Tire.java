package chapter08;

public interface Tire {
    public abstract void roll(String name);
}
