package chapter08.example;

public interface Soundable {
    public abstract String sound();
}
