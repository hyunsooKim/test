package chapter08.example;

public interface Action {
    public abstract void work();
}
