package chapter08.example;

public interface DataAccessObject {
    public abstract void select();
    public abstract void insert();
    public abstract void update();
    public abstract void delete();
}
