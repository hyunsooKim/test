package chapter08;

public interface Searchable {
    public abstract void search(String url);
}
