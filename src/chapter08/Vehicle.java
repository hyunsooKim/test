package chapter08;

public interface Vehicle {
    public abstract void run();
}
